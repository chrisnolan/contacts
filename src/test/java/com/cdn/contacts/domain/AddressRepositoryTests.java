package com.cdn.contacts.domain;

import com.cdn.contacts.Application;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.SimpleDateFormat;

/**
 * Created by Chris on 22/06/2014.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class AddressRepositoryTests {

    @Autowired
    AddressRepository addressRepository;

    @Autowired
    ContactRepository contactRepository;

    SimpleDateFormat sdf;
    Contact contactOne;
    Address contactOneAddress;

    @Before
    public void setUp() throws Exception {
        sdf = new SimpleDateFormat("dd/MM/yyyy");
        contactOne = new Contact("chrisnolan", "Chris", "Nolan", sdf.parse("2/12/1982"));
        contactOneAddress = new Address("Home", "45 Old Road", "Great Green", "Sittingstone", "Kent", "MR2 2EE", "UK", contactOne);

        contactRepository.save(contactOne);
        addressRepository.save(contactOneAddress);
    }

    @After
    public void tearDown() throws Exception {
        addressRepository.deleteAll();
        contactRepository.deleteAll();
    }

    @Test
    public void testRepository() throws Exception {
        Assert.assertNotNull(addressRepository);
    }

    @Test
    public void testContactRetrieval() throws Exception {
        Assert.assertEquals(contactOne.getId(), addressRepository.findByContact(contactOne).get(0).getId());
    }

    @Test
    public void testQueryByCountry() throws Exception {
        Assert.assertEquals(contactOneAddress.getAddressCountry(), addressRepository.findByAddressCountry(contactOneAddress.getAddressCountry()).get(0).getAddressCountry());
    }

    @Test
    public void testQueryByAddressName() throws Exception {
        Assert.assertEquals(contactOneAddress.getAddressName(), addressRepository.findByAddressNameContainingIgnoreCase(contactOneAddress.getAddressName().toLowerCase()).get(0).getAddressName());
    }

}
