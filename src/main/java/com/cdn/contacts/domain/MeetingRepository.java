package com.cdn.contacts.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Chris on 21/06/2014.
 */
public interface MeetingRepository extends CrudRepository<Meeting, Long> {

    List<Meeting> findByMeetingDateAfter(Date afterDate);
    List<Meeting> findByMeetingDate(Date meetingDate);
    //List<Meeting> findByMeetingDateAfterAndBefore(Date afterDate, Date beforeDate);
    List<Meeting> findByContact(Contact contact);

}
