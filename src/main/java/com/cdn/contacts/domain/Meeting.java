package com.cdn.contacts.domain;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * Domain model for a meeting
 *
 * Created by Chris on 21/06/2014.
 */
@Entity
public class Meeting {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private Date meetingDate;
    private String details;

    @CreatedDate
    private Date createdDate;

    //private long contactId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "contact_id")
    private Contact contact;

    protected Meeting() {}

    public Meeting(Date meetingDate, String details, Contact contact)
    {
        this.meetingDate = meetingDate;
        this.details = details;
        this.contact = contact;
    }

    public Date getMeetingDate() {
        return meetingDate;
    }

    public void setMeetingDate(Date meetingDate) {
        this.meetingDate = meetingDate;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public long getId() {
        return id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Contact getContact() {
        return contact;
    }

    @Override
    public String toString() {
        return "Meeting{" +
                "id=" + id +
                ", meetingDate=" + meetingDate +
                ", details='" + details + '\'' +
                ", createdDate=" + createdDate +
                ", contact=" + contact +
                '}';
    }
}
