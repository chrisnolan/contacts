package com.cdn.contacts.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Customer repository extending Spring JPA CRUD Repository
 *
 * Created by Chris on 21/06/2014.
 */
public interface ContactRepository extends CrudRepository<Contact, Long> {

    List<Contact> findByLastName(String lastName);
    List<Contact> findByUsername(String username);

}
