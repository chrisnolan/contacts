package com.cdn.contacts.domain;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * Domain model for an address
 *
 * Created by Chris on 21/06/2014.
 */
@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String addressName;
    private String addressLineOne;
    private String addressLineTwo;
    private String addressCity;
    private String addressCounty;
    private String addressPostCode;
    private String addressCountry;

    @CreatedDate
    private Date createdDate;

    @ManyToOne(optional = false)
    @JoinColumn(name = "contact_id")
    private Contact contact;

    protected Address() {}

    public Address(String addressName, Contact contact)
    {
        this.contact = contact;
        this.addressName = addressName;
    }

    public Address(String addressName, String addressLineOne, String addressLineTwo, String addressCity, String addressCounty, String addressPostCode, String addressCountry, Contact contact) {
        this.addressName = addressName;
        this.addressLineOne = addressLineOne;
        this.addressLineTwo = addressLineTwo;
        this.addressCity = addressCity;
        this.addressCounty = addressCounty;
        this.addressPostCode = addressPostCode;
        this.addressCountry = addressCountry;
        this.contact = contact;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getAddressLineOne() {
        return addressLineOne;
    }

    public void setAddressLineOne(String addressLineOne) {
        this.addressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressCounty() {
        return addressCounty;
    }

    public void setAddressCounty(String addressCounty) {
        this.addressCounty = addressCounty;
    }

    public String getAddressPostCode() {
        return addressPostCode;
    }

    public void setAddressPostCode(String addressPostCode) {
        this.addressPostCode = addressPostCode;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public long getId() {
        return id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Contact getContact() {
        return contact;
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", addressName='" + addressName + '\'' +
                ", addressLineOne='" + addressLineOne + '\'' +
                ", addressLineTwo='" + addressLineTwo + '\'' +
                ", addressCity='" + addressCity + '\'' +
                ", addressCounty='" + addressCounty + '\'' +
                ", addressPostCode='" + addressPostCode + '\'' +
                ", addressCountry='" + addressCountry + '\'' +
                ", createdDate=" + createdDate +
                ", contact=" + contact +
                '}';
    }
}
