package com.cdn.contacts.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Chris on 21/06/2014.
 */
public interface AddressRepository extends CrudRepository<Address, Long> {

    List<Address> findByAddressCountry(String addressCountry);
    List<Address> findByAddressNameContainingIgnoreCase(String addressName);
    List<Address> findByContact(Contact contact);

}
