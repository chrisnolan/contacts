package com.cdn.contacts.domain;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Chris on 22/06/2014.
 */
@Entity
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private String messageSubject;
    private String messageContents;

    @CreatedDate
    private Date createdDate;

    private Date messageReadDate;
    private Boolean messageRead;

    @ManyToOne(optional = false)
    @JoinColumn(name = "contact_id")
    private Contact contact;

    protected Message() {}

    public Message(String messageSubject, String messageContents, Contact contact)
    {
        this.messageSubject = messageSubject;
        this.messageContents = messageContents;
        this.contact = contact;
        this.messageRead = false;
    }

    public String getMessageSubject() {
        return messageSubject;
    }

    public void setMessageSubject(String messageSubject) {
        this.messageSubject = messageSubject;
    }

    public String getMessageContents() {
        return messageContents;
    }

    public void setMessageContents(String messageContents) {
        this.messageContents = messageContents;
    }

    public Date getMessageReadDate() {
        return messageReadDate;
    }

    public void setMessageReadDate(Date messageReadDate) {
        this.messageReadDate = messageReadDate;
    }

    public Boolean getMessageRead() {
        return messageRead;
    }

    public void setMessageRead(Boolean messageRead) {
        this.messageRead = messageRead;
    }

    public long getId() {
        return id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Contact getContact() {
        return contact;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", messageSubject='" + messageSubject + '\'' +
                ", messageContents='" + messageContents + '\'' +
                ", createdDate=" + createdDate +
                ", messageReadDate=" + messageReadDate +
                ", messageRead=" + messageRead +
                ", contact=" + contact +
                '}';
    }
}
