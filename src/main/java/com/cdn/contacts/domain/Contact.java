package com.cdn.contacts.domain;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

/**
 * Domain model for a contact
 *
 * Created by Chris on 21/06/2014.
 */

@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String firstName;
    private String lastName;

    @Column(unique = true)
    private String username;

    private Date dateOfBirth;

    @CreatedDate
    private Date createdDate;

    @OneToMany(mappedBy = "contact", targetEntity = Meeting.class,
    fetch = FetchType.EAGER)
    private Collection<Meeting> meetings;

    @OneToMany(mappedBy = "contact", targetEntity = Address.class,
    fetch = FetchType.EAGER)
    private Collection<Address> addresses;

    @OneToMany(mappedBy = "contact", targetEntity = Message.class,
    fetch = FetchType.EAGER)
    private Collection<Message> messages;

    protected Contact() {}

    public Contact(String username, String firstName, String lastName) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Contact(String username, String firstName, String lastName, Date dateOfBirth) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public Collection<Meeting> getMeetings() { return meetings; }

    public Collection<Address> getAddresses() { return addresses; }

    public Collection<Message> getMessages() { return messages; }

    public String getName() {
        return firstName + " " + lastName;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", createdDate=" + createdDate +
                ", meetings=" + meetings +
                ", addresses=" + addresses +
                ", messages=" + messages +
                '}';
    }
}
