package com.cdn.contacts.domain;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Chris on 22/06/2014.
 */
public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findByCreatedDateAfter(Date afterDate);
    List<Message> findByCreatedDate(Date createdDate);
    List<Message> findByContact(Contact contact);
    List<Message> findByMessageRead(Boolean read);

}
