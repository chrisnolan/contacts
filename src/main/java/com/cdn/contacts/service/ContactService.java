package com.cdn.contacts.service;

import com.cdn.contacts.domain.Contact;

/**
 * Created by Chris on 21/06/2014.
 */
public interface ContactService {

    Iterable<Contact> findAll();
    Contact findById(long id);
    Contact save(Contact contact);

}
