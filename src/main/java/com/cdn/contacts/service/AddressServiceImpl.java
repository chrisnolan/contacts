package com.cdn.contacts.service;

import com.cdn.contacts.domain.Address;
import com.cdn.contacts.domain.AddressRepository;
import com.cdn.contacts.domain.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chris on 21/06/2014.
 */
@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    @Autowired
    public AddressServiceImpl(final AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Iterable<Address> findAll() {
        return addressRepository.findAll();
    }

    @Override
    public Iterable<Address> findAllForContact(Contact contact) {
        return addressRepository.findByContact(contact);
    }

    @Override
    public Address save(Address address) {
        return addressRepository.save(address);
    }
}
