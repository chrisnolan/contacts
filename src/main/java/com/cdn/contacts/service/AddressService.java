package com.cdn.contacts.service;

import com.cdn.contacts.domain.Address;
import com.cdn.contacts.domain.Contact;
import org.springframework.stereotype.Service;

/**
 * Created by Chris on 21/06/2014.
 */
public interface AddressService {

    Iterable<Address> findAll();
    Iterable<Address> findAllForContact(Contact contact);
    Address save(Address address);

}
