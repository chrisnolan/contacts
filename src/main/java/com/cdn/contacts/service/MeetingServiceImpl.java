package com.cdn.contacts.service;

import com.cdn.contacts.domain.Contact;
import com.cdn.contacts.domain.Meeting;
import com.cdn.contacts.domain.MeetingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chris on 21/06/2014.
 */
@Service
public class MeetingServiceImpl implements MeetingService {

    private final MeetingRepository meetingRepository;

    @Autowired
    public MeetingServiceImpl(final MeetingRepository meetingRepository) {
        this.meetingRepository = meetingRepository;
    }


    @Override
    public Iterable<Meeting> findAll() {
        return meetingRepository.findAll();
    }

    @Override
    public Iterable<Meeting> findAllForContact(Contact contact) {
        return meetingRepository.findByContact(contact);
    }

    @Override
    public Meeting save(Meeting meeting) {
        return meetingRepository.save(meeting);
    }
}
