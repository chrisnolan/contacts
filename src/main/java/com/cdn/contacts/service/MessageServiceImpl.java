package com.cdn.contacts.service;

import com.cdn.contacts.domain.Contact;
import com.cdn.contacts.domain.Message;
import com.cdn.contacts.domain.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chris on 22/06/2014.
 */
@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Autowired
    public MessageServiceImpl(final MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    @Override
    public Iterable<Message> findAll() {
        return messageRepository.findAll();
    }

    @Override
    public Iterable<Message> findAllForContact(Contact contact) {
        return messageRepository.findByContact(contact);
    }

    @Override
    public Message save(Message message) {
        return messageRepository.save(message);
    }
}
