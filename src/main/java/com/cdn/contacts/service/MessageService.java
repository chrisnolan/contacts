package com.cdn.contacts.service;

import com.cdn.contacts.domain.Contact;
import com.cdn.contacts.domain.Message;

/**
 * Created by Chris on 22/06/2014.
 */
public interface MessageService {

    Iterable<Message> findAll();
    Iterable<Message> findAllForContact(Contact contact);

    Message save(Message message);


}
