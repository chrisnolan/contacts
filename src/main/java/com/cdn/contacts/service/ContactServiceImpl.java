package com.cdn.contacts.service;

import com.cdn.contacts.domain.Contact;
import com.cdn.contacts.domain.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Chris on 21/06/2014.
 */
@Service
public class ContactServiceImpl implements ContactService {

    private final ContactRepository contactRepository;

    @Autowired
    public ContactServiceImpl(final ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    @Override
    public Iterable<Contact> findAll() {
        return contactRepository.findAll();
    }

    @Override
    public Contact findById(long id) {
        return contactRepository.findOne(id);
    }

    @Override
    public Contact save(Contact contact) {
        // TODO: validation and security checks
        return contactRepository.save(contact);
    }


}
