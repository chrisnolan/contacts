package com.cdn.contacts.service;

import com.cdn.contacts.domain.Contact;
import com.cdn.contacts.domain.Meeting;

/**
 * Created by Chris on 21/06/2014.
 */
public interface MeetingService {

    Iterable<Meeting> findAll();
    Iterable<Meeting> findAllForContact(Contact contact);
    Meeting save(Meeting meeting);

}
