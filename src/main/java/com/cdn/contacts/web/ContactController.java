package com.cdn.contacts.web;

import com.cdn.contacts.domain.Contact;
import com.cdn.contacts.domain.ContactRepository;
import com.cdn.contacts.domain.Meeting;
import com.cdn.contacts.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 * Created by Chris on 21/06/2014.
 */
@Controller
@RequestMapping("/contacts")
public class ContactController {

    private final ContactService contactService;

    @Autowired
    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @RequestMapping
    public ModelAndView index() {
        Iterable<Contact> contacts = contactService.findAll();
        return new ModelAndView("contacts/index", "contacts", contacts);
    }

    @RequestMapping(value = "new", method = RequestMethod.GET)
    public String newContact(@ModelAttribute("contact") Contact contact) {
        return "contacts/new";
    }

    @RequestMapping(value = "new", method = RequestMethod.POST)
    public String newContactPost(@ModelAttribute("contact") Contact contact, BindingResult bindingResult) {
        System.out.println(contact.toString());
        if(bindingResult.hasErrors()) {
            return "contacts/new";
        }
        contactService.save(contact);
        return "redirect:/contacts";
    }

    @RequestMapping("{contact}/meetings")
    public ModelAndView meetingsForContact(@PathVariable Long contact) {
        Contact foundContact = contactService.findById(contact);
        return new ModelAndView("contacts/meetings", "contact", foundContact);
    }

    @RequestMapping("{contact}/addresses")
    public ModelAndView addressesForContact(@PathVariable Long contact) {
        Contact foundContact = contactService.findById(contact);
        return new ModelAndView("contacts/addresses", "contact", foundContact);
    }

    @RequestMapping("{contact}/messages")
    public ModelAndView messagesForContact(@PathVariable Long contact) {
        Contact foundContact = contactService.findById(contact);
        return new ModelAndView("contacts/messages", "contact", foundContact);
    }

}
