package com.cdn.contacts.web;

import com.cdn.contacts.domain.Address;
import com.cdn.contacts.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chris on 21/06/2014.
 */
@Controller
@RequestMapping("/addresses")
public class AddressController {

    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @RequestMapping
    public ModelAndView index() {
        Iterable<Address> addresses = addressService.findAll();
        return new ModelAndView("addresses/index", "addresses", addresses);
    }

}
