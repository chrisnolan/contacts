package com.cdn.contacts.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Chris on 21/06/2014.
 */
@Controller
public class SiteController {

    @RequestMapping("/")
    public String index()
    {
        return "index";
    }

    @RequestMapping("/login")
    public String login() { return "login"; }

}
