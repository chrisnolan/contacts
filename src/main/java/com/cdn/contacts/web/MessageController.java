package com.cdn.contacts.web;

import com.cdn.contacts.domain.Message;
import com.cdn.contacts.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chris on 22/06/2014.
 */

@Controller
@RequestMapping("/messages")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public  MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    public ModelAndView index() {
        Iterable<Message> messages = messageService.findAll();
        return new ModelAndView("messages/index", "messages", messages);
    }

}
