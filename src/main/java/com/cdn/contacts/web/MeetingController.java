package com.cdn.contacts.web;

import com.cdn.contacts.domain.Meeting;
import com.cdn.contacts.service.MeetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Chris on 21/06/2014.
 */
@Controller
@RequestMapping("/meetings")
public class MeetingController {

    private final MeetingService meetingService;

    @Autowired
    public MeetingController(MeetingService meetingService) {
        this.meetingService = meetingService;
    }

    @RequestMapping
    public ModelAndView index() {
        Iterable<Meeting> meetings = meetingService.findAll();
        return new ModelAndView("meetings/index", "meetings", meetings);
    }

}
