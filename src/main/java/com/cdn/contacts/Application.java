package com.cdn.contacts;


import com.cdn.contacts.domain.*;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Main Application context to allow running via built in Jetty container
 *
 * Created by Chris on 21/06/2014.
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan
public class Application {

    static Logger logger = Logger.getLogger(Application.class.getName());

    public static void main(String[] args)
    {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        configureDefaults(ctx);
    }

    private static void configureDefaults(ApplicationContext ctx) {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        try {
            ContactRepository contactRepository = ctx.getBean(ContactRepository.class);
            MeetingRepository meetingRepository = ctx.getBean(MeetingRepository.class);
            AddressRepository addressRepository = ctx.getBean(AddressRepository.class);


            Contact chrisnolan = contactRepository.save(new Contact("chrisnolan", "Chris", "Nolan", sdf.parse("2/12/1982")));
            Meeting chrisMeeting = meetingRepository.save(new Meeting(sdf.parse("20/06/2014"), "A test Meeting", chrisnolan));
            Address chrisAddress = addressRepository.save(new Address("Home", "45 Old Road", "Great Green", "Sittingstone", "Kent", "MR2 2EE", "UK", chrisnolan));

            contactRepository.save(new Contact("mikebrown", "Mike", "Brown", sdf.parse("4/2/1989")));
        }
        catch(ParseException ex)
        {
            logger.severe("Parse exception");
        }

    }

}
